const path = require('path')

module.exports = function createPages({ actions, graphql }) {
  const { createPage } = actions

  const blogPostTemplate = path.resolve(`src/templates/post-template.js`)

  return graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        edges {
          node {
            excerpt
            frontmatter {
              path
              category
              title
              thumbnails {
                childImageSharp {
                  works: sizes(maxWidth: 800, maxHeight: 300, quality: 100) {
                    src
                  }
                  blogs: sizes(maxWidth: 45, maxHeight: 45, quality: 100) {
                    src
                  }
                }
              }
            }
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors)
    }

    const posts = result.data.allMarkdownRemark.edges

    result.data.allMarkdownRemark.edges.forEach(({ node }, index) => {
      createPage({
        path: node.frontmatter.path,
        component: blogPostTemplate,
        context: {
          next: posts.filter(
            post =>
              post.node.frontmatter.category == node.frontmatter.category &&
              post.node.frontmatter.path != node.frontmatter.path
          ),
        }, // additional data can be passed via context
      })
    })
  })
}
