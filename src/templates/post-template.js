import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import { rhythm } from '../utils/typography'
import styled, { injectGlobal } from 'styled-components'
import Layout from '../components/Layout'
import Post from '../components/Post'

const PostWrapper = styled.div`
  background: #fff;
  width: 100%;
  height: 100%;
`

injectGlobal`
  .post h1{
    font-size: var(--h1-blog-text-size);
    color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
    font-family: var(--primary-font);  
    padding-top: 32px;
  }
  .post-content p{
    font-family: var(--secondary-font);
    font-size: var(--h4-txt-size);
    line-height: ${rhythm(1.2)};
  }
  .post-content ul, .post-content li, .post-content ol {
    font-size: var(--h4-txt-size);
    line-height: ${rhythm(1.1)};
  }
`

export default function Template({
  data = {},
  location,
  pageContext,
  ...rest
}) {
  const { markdownRemark: post } = data
  const { next } = pageContext

  return (
    <Layout>
      <PostWrapper>
        <Helmet title={`${post.frontmatter.title} on aswin.design`} />
        {/* <div className="progress-bar" style={style} /> */}
        <Post
          html={post.html}
          title={post.frontmatter.title}
          cover={post.frontmatter.cover.childImageSharp.sizes}
          next={next}
          path={post.frontmatter.path}
          category={post.frontmatter.category}
        />
      </PostWrapper>
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        cover {
          childImageSharp {
            sizes(maxWidth: 1695, maxHeight: 750, quality: 100) {
              ...GatsbyImageSharpSizes
            }
          }
        }
        thumbnails {
          childImageSharp {
            sizes(maxWidth: 45, maxHeight: 45, quality: 100) {
              ...GatsbyImageSharpSizes
            }
          }
        }
        path
        title
        category
      }
    }
  }
`
