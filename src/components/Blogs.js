import React from 'react'
import { H2, H4Italic, P, A, HomeTitle } from '../utils/typography'
import styled from 'styled-components'
import Article from './Article'
import { media } from '../utils/responsive'
import _ from 'lodash'
import { StaticQuery, graphql } from 'gatsby'
import { getNodesOfType } from '../utils/graphql'

const BlogsSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const StyledRow = styled.div`
  display: flex;
  flex-direction: column;
`

const Row = ({ blogs }) => (
  <StyledRow>
    {blogs.map((blog, i) => (
      <Article
        key={i}
        title={blog.node.frontmatter.title}
        description={blog.node.excerpt}
        path={blog.node.frontmatter.path}
        thumbnail={
          blog.node.frontmatter.thumbnails[0].childImageSharp.sizes.src
        }
      />
    ))}
  </StyledRow>
)

function getBlogs(blogs) {
  let allblogs = _.chunk(blogs, 3).reverse()
  return allblogs
}

const BlogsJSX = ({ data }) => (
  <BlogsSection>
    <HomeTitle>Blogs & Artcles</HomeTitle>
    {getBlogs(getNodesOfType(data, 'blog')).map((blogs, i) => (
      <Row key={i} blogs={blogs} />
    ))}
  </BlogsSection>
)

export default function Blogs() {
  return (
    <StaticQuery
      query={graphql`
        query BlogThumbnailsQuery {
          allMarkdownRemark(
            sort: { order: DESC, fields: [frontmatter___date] }
          ) {
            edges {
              node {
                ...BlogThumbnail
              }
            }
          }
        }
      `}
      render={data => <BlogsJSX data={data} />}
    />
  )
}
