import React from 'react'
import { Link } from 'gatsby'
import { Container, media } from '../utils/responsive'
import { rhythm, A } from '../utils/typography'
import logo from '../images/logo.png'
import styled from 'styled-components'
import Button from './Button'
import { openCrisp } from '../utils/crisp'
import Resume from '../documents/Resume.pdf'

const Logo = styled.img`
  height: 1.25rem;
  margin-top: 1.2rem;
  margin-bottom: 1.2rem;
  display: block;
`

const Nav = styled.nav`
  display: flex;

  border-bottom: 1px solid #ffffff0d;
  align-items: center;
  min-height: ${rhythm(2.8)};
  & button {
    margin-left: 1.8rem;
  }
`

const ANav = styled(Link)`
  font-family: var(--secondary-font);
  /* font-size: 1.2rem; */
  font-weight: var(--secondary-font-light);
  display: inline-block;
  color: var(--main-txt-color);
  opacity: 0.8;
  text-decoration: none;
  transition: 0.3s;
  margin-left: ${rhythm(1.1)};
  &:hover {
    color: var(--primary-color);
  }
`

const ANavResume = styled(A)`
  font-family: var(--secondary-font);
  /* font-size: 0.8rem; */
  font-weight: var(--secondary-font-light);
  display: inline-block;
  color: var(--main-txt-color);
  opacity: 0.8;
  text-decoration: none;
  transition: 0.3s;
  margin-left: ${rhythm(1.1)};
  &:hover {
    color: var(--primary-color);
  }
`

const ResponsiveLinks = styled.span`
  display: none;
  ${media.tablet`
  display: block;

  `};
`

const Navbar = styled.div`
  background: var(--main-bg-color);
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 5;
`

const Header = ({ siteTitle }) => (
  <Navbar>
    <Container>
      <Nav>
        <Link
          to="/"
          style={{
            color: 'white',
            textDecoration: 'none',
            marginRight: 'auto',
          }}
        >
          <Logo src={logo} alt="logo" className="grow" />
          {/* {siteTitle} */}
        </Link>
        <ResponsiveLinks>
          <ANav to="/">Home</ANav>

          {/* <ANav
            to="/"
            style={{
              color: 'white',
              textDecoration: 'none',
            }}
          >
            Projects
          </ANav>

          <ANav
            to="/"
            style={{
              color: 'white',
              textDecoration: 'none',
            }}
          >
            Blogs
          </ANav> */}
          <ANavResume href={Resume} download="Resume.pdf">
            📥 Download Resume
          </ANavResume>
        </ResponsiveLinks>
        <div>
          <Button onClick={openCrisp}>🏖 Hire me</Button>
        </div>
      </Nav>
    </Container>
  </Navbar>
)

export default Header
