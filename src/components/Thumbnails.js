import { graphql } from 'gatsby'

export const workThumbnails = graphql`
  fragment WorkThumbnail on MarkdownRemark {
    excerpt(pruneLength: 100)
    frontmatter {
      title
      date(formatString: "MMMM DD, YYYY")
      path
      category
      thumbnails {
        childImageSharp {
          sizes(maxWidth: 1600, maxHeight: 600, quality: 100) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`

export const blogThumbnails = graphql`
  fragment BlogThumbnail on MarkdownRemark {
    excerpt(pruneLength: 100)
    frontmatter {
      title
      date(formatString: "MMMM DD, YYYY")
      path
      category
      thumbnails {
        childImageSharp {
          sizes(maxWidth: 45, maxHeight: 45, quality: 100) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`
