import React from 'react'
import Slider from 'react-slick'
import styled from 'styled-components'
import Img from 'gatsby-image'

const StyledImg = styled(Img)`
  cursor: -webkit-grab;
  cursor: grab;
  &:active {
    cursor: grabbing;
  }
  outline: 0;
`

class ImageSlider extends React.Component {
  render() {
    var { images } = this.props

    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
    }
    return (
      <Slider {...settings}>
        {images.map((image, i) => (
          <StyledImg sizes={image} key={i} alt="zeta" />
        ))}
      </Slider>
    )
  }
}

export default ImageSlider
