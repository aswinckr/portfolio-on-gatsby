import styled from 'styled-components'

// Todo - Button width is uneven on smaller screens

const Button = styled.button`
  border-radius: 6px;
  border: 1px solid var(--primary-color);
  background-color: transparent;
  color: ${props => (props.black ? '#000' : '#FFF')};
  /* font-size: var(--btn-font-size); */
  font-weight: var(--p-normal-weight);
  padding: var(--btn-padding);
  transition: var(--btn-hover-transition);
  min-width: 115px;
  &:hover {
    color: var(--primary-color);
    cursor: pointer;
  }
  &:focus {
    outline: none;
  }
`

export default Button
