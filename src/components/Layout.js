import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Header from './Header'
import { StaticQuery, graphql } from 'gatsby'
import Footer from './Footer'
import PageTransition from 'gatsby-plugin-page-transitions'

import '../css/base.css'

class Layout extends Component {
  render() {
    const { children, pageContext } = this.props
    return (
      <StaticQuery
        query={graphql`
          query SiteTitleQuery {
            site {
              siteMetadata {
                title
              }
            }
          }
        `}
        render={data => (
          <>
            <Helmet
              title={data.site.siteMetadata.title}
              meta={[
                { name: 'description', content: 'Aswins Blog and Portfolio' },
                {
                  name: 'keywords',
                  content: 'portfolio, ux, designer, india, freelancer, design',
                },
              ]}
            >
              <html lang="en" />
            </Helmet>
            <Header siteTitle={data.site.siteMetadata.title} />

            <div style={{ marginTop: '2.8rem' }}>{children}</div>
            <Footer />
          </>
        )}
      />
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
