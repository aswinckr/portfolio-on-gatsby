import React, { Component } from 'react'
import { H2, H4Italic, P, A, HomeTitle } from '../utils/typography'
import styled from 'styled-components'
import { media } from '../utils/responsive'
import Work from './Work'
import Reveal from 'react-reveal/Reveal'
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'
import { getNodesOfType } from '../utils/graphql'

const WorksSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const Subheading = styled(H4Italic)`
  max-width: 70%;
`

const WorksBody = styled.div`
  display: flex;
  padding: 50px calc(50% - 400px);
  justify-content: space-between;
  flex-direction: column;
  & p {
    ${media.desktop`width: 50%;`} ${media.desktop`padding: 0 26px;`};
  }
`

// function getNodesOfType(data, type) {
//   const edges = data.allMarkdownRemark.edges
//   const nodes = edges.filter(
//     eachnode => eachnode.node.frontmatter.category == type
//   )
//   return nodes
// }

function next(data, path) {
  const nodes = getNodesOfType(data, 'work')
  const next = nodes.filter(node => node.node.frontmatter.path != path)
  return next
}

export function getImages(project) {
  const thumbnailArrays = project.node.frontmatter.thumbnails

  let images = []
  thumbnailArrays.map((thumbnail, i) => {
    images.push(thumbnail.childImageSharp.sizes)
  })

  return images
}

function getThumbnail(project) {
  const thumbnail =
    project.node.frontmatter.thumbnails[0].childImageSharp.sizes.src
  return thumbnail
}

const WorksJSX = ({ data }) => (
  <WorksSection>
    <HomeTitle>Work & Case Studies</HomeTitle>
    {getNodesOfType(data, 'work').map((project, i) => (
      <Work
        key={i}
        title={project.node.frontmatter.title}
        subtitle={'Business facing web app made for Zeta'}
        description={project.node.excerpt}
        cta={'See Case Study'}
        link={project.node.frontmatter.path}
        thumbnail={getThumbnail(project)}
        next={next(data, project.node.frontmatter.path)}
      />
    ))}
  </WorksSection>
)

export default function Works() {
  return (
    <StaticQuery
      query={graphql`
        query WorkThumbnailsQuery {
          allMarkdownRemark(
            sort: { order: DESC, fields: [frontmatter___date] }
          ) {
            edges {
              node {
                ...WorkThumbnail
              }
            }
          }
        }
      `}
      render={data => <WorksJSX data={data} />}
    />
  )
}
