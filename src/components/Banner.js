import React from 'react'
import { H1, H3, A, Caption, ANav, P } from '../utils/typography'
import styled from 'styled-components'
import Button from './Button'
import { Link } from 'gatsby'

const WelcomeWrapper = styled.div`
  text-align: left;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: 4px;
  margin-bottom: 32px;
`

const Banner = props => (
  <header>
    <WelcomeWrapper>
      {/* { if props.referrer {<H1>Welcome Home, {props.referrer}</H1>} } */}
      {props.referrer ? (
        <H1>{`Hello, ${props.referrer}`} 👋🏼 </H1>
      ) : (
        <H1>Hey there </H1>
      )}

      <P>
        I'm Aswin, designer and self-taught developer living in Singapore. I
        currently work with{' '}
        <A href="https://grab.com" target="_blank">
          @Grab
        </A>{' '}
        as a Senior Product Designer with Financial / Payment Services.
        Previously I used to freelance for a couple of years and I've worked
        with some popular startups like <A href="https://zeta.in">@Zeta</A> &{' '}
        <A href="https://housing.com">@Housing</A>. I design for both web and
        mobile but knowing React gives me an edge over web applications. Hire me
        to review your designs, teach you some techniques or train at an event.
        Feel free to look at my work, read my blog or get in touch!
      </P>
    </WelcomeWrapper>
  </header>
)

export default Banner
