import React, { Component } from 'react'
import { BlogContainer, media } from '../utils/responsive'
import { rhythm, H4, HomeTitle, P } from '../utils/typography'
import styled, { injectGlobal } from 'styled-components'
import Work from '../components/Work'
import Article from '../components/Article'
import ScrollProgress from 'scrollprogress'
import ScrollToTop from 'react-scroll-up'
import Img from 'gatsby-image'

const CoverImage = styled.div`
  position: relative;
`

const Next = styled.div`
  background: var(--main-bg-color);
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const WorkWrapper = styled.div`
  max-width: 315px;
  margin: 36px auto;
`

const ScrollTopArrow = styled.div`
  position: relative;
  right: -15px;
  bottom: -20px;

  ${media.tablet`
z-index: 1000;

  right: 0;
  bottom: 0;

  `};
`

injectGlobal`
  .post h1{
    font-size: var(--h1-blog-text-size);
    color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
    font-family: var(--primary-font);
    padding-top: 60px;
  }

  .post h2{
    padding-top: 32px;
  }


  .post-content p{
    font-family: var(--secondary-font);
    font-size: var(--h4-txt-size);
    line-height: ${rhythm(1.2)};
  }
  .post-content ul, .post-content li, .post-content ol {
    font-size: var(--h4-txt-size);
    line-height: ${rhythm(1.1)};
  }
`

function getThumbnail(project) {
  const thumbnail =
    project.node.frontmatter.thumbnails[0].childImageSharp.works.src
  return thumbnail
}

class Post extends Component {
  constructor(props) {
    super(props)
    this.state = {
      progress: 0,
    }
  }

  componentDidMount() {
    const { category } = this.props
    if (category == 'blog') {
      ;(function() {
        // DON'T EDIT BELOW THIS LINE
        var d = document,
          s = d.createElement('script')
        s.src = 'https://aswinckr.disqus.com/embed.js'
        s.setAttribute('data-timestamp', +new Date())
        ;(d.head || d.body).appendChild(s)
      })()
    }

    this.progressObserver = new ScrollProgress((x, y) => {
      this.setState({ progress: y * 100 })
    })
  }

  componentWillUnmount() {
    this.progressObserver.destroy()
  }

  render() {
    const {
      html: __html,
      post,
      next,
      title,
      cover,
      path,
      category,
    } = this.props
    const style = {
      backgroundColor: 'var(--primary-color)',
      height: '3px',
      position: 'fixed',
      top: 0,
      zIndex: 100,
      width: `${this.state.progress}%`,
    }
    const scrolltopstyle = {
      bottom: 100,
    }
    return (
      <>
        <div className="progress-bar" style={style} />
        <CoverImage>
          <Img sizes={cover} alt="cover" />
        </CoverImage>
        <ScrollToTop showUnder={100} style={scrolltopstyle}>
          <ScrollTopArrow>
            <svg
              width="52px"
              height="54px"
              viewBox="0 0 52 54"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
            >
              <g
                id="Design"
                stroke="none"
                strokeWidth="1"
                fill="none"
                fillRule="evenodd"
              >
                <g id="Artboard" transform="translate(-136.000000, -99.000000)">
                  <g id="Group" transform="translate(136.000000, 99.000000)">
                    <ellipse
                      id="Oval-Copy-4"
                      stroke="#26B9A0"
                      cx="26"
                      cy="26.7103825"
                      rx="25.5"
                      ry="26.2103825"
                    />
                    <path
                      d="M19.3743277,23.3294799 C23.9494102,18.7543974 26.2369514,16.4668562 26.2369514,16.4668562 C26.2369514,16.4668562 26.2369514,23.6322145 26.2369514,37.9629311"
                      id="Path-3"
                      stroke="#39C0A9"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M26.3743277,23.3294799 C30.9494102,18.7543974 33.2369514,16.4668562 33.2369514,16.4668562 C33.2369514,16.4668562 33.2369514,23.6322145 33.2369514,37.9629311"
                      id="Path-3-Copy"
                      stroke="#39C0A9"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      transform="translate(29.805640, 27.214894) scale(-1, 1) translate(-29.805640, -27.214894) "
                    />
                  </g>
                </g>
              </g>
            </svg>
          </ScrollTopArrow>
        </ScrollToTop>

        <BlogContainer className="post-container">
          <div className="post">
            <h1>{title}</h1>
            <div
              className="post-content"
              dangerouslySetInnerHTML={{ __html }}
            />
          </div>
          {category == 'blog' ? (
            <>
              <div id="disqus_thread" />
              <script
                dangerouslySetInnerHTML={{
                  __html: `var disqus_config = function () {
this.page.url = ${path};  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = ${path}; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};`,
                }}
              />
            </>
          ) : (
            ''
          )}
        </BlogContainer>
        {next.length > 0 ? (
          <Next>
            <WorkWrapper>
              <HomeTitle>Suggested next for you</HomeTitle>
              {next[0].node.frontmatter.category === 'work' ? (
                <Work
                  title={next[0].node.frontmatter.title}
                  subtitle={'Business facing web app made for Zeta'}
                  description={next[0].node.excerpt}
                  cta={'See Case Study'}
                  link={next[0].node.frontmatter.path}
                  thumbnail={getThumbnail(next[0])}
                  style={{ maxWidth: '320px' }}
                />
              ) : (
                <div className="article">
                  <Article
                    title={next[0].node.frontmatter.title}
                    description={next[0].node.excerpt}
                    path={next[0].node.frontmatter.path}
                    thumbnail={
                      next[0].node.frontmatter.thumbnails[0].childImageSharp
                        .blogs.src
                    }
                  />
                </div>
              )}
            </WorkWrapper>
          </Next>
        ) : (
          ''
        )}
      </>
    )
  }
}

export default Post
