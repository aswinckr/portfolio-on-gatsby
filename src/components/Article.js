import React from 'react'
import styled from 'styled-components'
import { P, rhythm } from '../utils/typography'
import { Link } from 'gatsby'

const ArticleWrapper = styled.div`
  display: flex;
  & div:nth-child(1) {
    order: ${props => (props.reverse ? 2 : 0)};
  }
`

const DescWrapper = styled.div`
  max-width: 80%;
`

const ArticleTitle = styled(Link)`
  font-family: var(--secondary-font);
  display: inline-block;
  font-size: var(--a-txt-size);
  color: ${props => (props.black ? '#000' : '#FFF')};
  text-decoration: none;
  transition: 0.3s;
  line-height: ${rhythm(1.5)};
  &:hover {
    color: var(--primary-color);
  }
`

const Article = ({ thumbnail, reverse, title, description, path, ...rest }) => (
  <ArticleWrapper reverse={reverse}>
    <Link to={path} style={{ textDecoration: 'none' }}>
      <img
        src={thumbnail}
        alt="blog-thumbnail"
        style={{ marginRight: 15, marginTop: 10 }}
      />
    </Link>
    <DescWrapper>
      <ArticleTitle to={path} style={{ textDecoration: 'none' }}>
        {title}
      </ArticleTitle>
      <P>{description}</P>
    </DescWrapper>
  </ArticleWrapper>
)

export default Article
