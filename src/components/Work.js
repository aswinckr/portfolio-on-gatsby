import React from 'react'
import styled from 'styled-components'
import { media } from '../utils/responsive'
import { H3, H4, H4Italic, P, ABlog } from '../utils/typography'
import { Link } from 'gatsby'
import { rhythm } from '../utils/typography'

const Wrapper = styled.div`
  text-align: left;
  padding: 0 calc(50% - 550px);
  display: flex;
  flex-direction: column;
  margin-top: 10px;
  ${media.desktop`
    
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin-bottom: 32px;
    `};
`

const Thumbnail = styled.div`
  text-align: center;
  width: 100%;
  background: url('${props => props.image}')
`

const ThumbnailImg = styled.img`
  margin-bottom: 0.7rem;
`

const Info = styled.div`
  ${media.desktop`
    padding-top: 0;
    padding-bottom: 0;`};
`

const WorkTitle = styled(Link)`
  font-family: var(--secondary-font);
  display: inline-block;
  font-size: var(--h4-text-size);
  color: ${props => (props.black ? '#000' : '#FFF')};
  text-decoration: none;
  transition: 0.3s;
  line-height: ${rhythm(1.5)};
  &:hover {
    color: var(--primary-color);
  }
`

class Work extends React.Component {
  render() {
    const { link, thumbnail } = this.props
    return (
      <Wrapper>
        <Link to={link} style={{ textDecoration: 'none' }}>
          <Thumbnail>
            <ThumbnailImg src={thumbnail} alt="zeta" />
          </Thumbnail>
        </Link>
        <Info>
          <WorkTitle to={link} style={{ textDecoration: 'none' }}>
            {this.props.title}
          </WorkTitle>
          <P>{this.props.description}</P>
        </Info>
      </Wrapper>
    )
  }
}

export default Work
