import React from 'react'
import { Link } from 'gatsby'
import Banner from '../components/Banner'
import About from '../components/About'
import Works from '../components/Works'
import Blogs from '../components/Blogs'
import { Container } from '../utils/responsive'
import Layout from '../components/Layout'
import styled from 'styled-components'
import { media } from '../utils/responsive'
import { getParams } from '../utils/graphql'

const Columns = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 4rem;
  ${media.tablet`   
    flex-direction: row;
    `};
  ${media.desktop`   
    flex-direction: row;
    `};
`

const Column = styled.div`
  flex: ${props => props.flex};
  padding-right: 0;
  padding-left: 0;
  margin-top: 24px;

  ${media.tablet`padding-right: ${props =>
    props.last ? 0 : '32px'};`} ${media.desktop`padding-left: ${props =>
    props.first ? 0 : '32px'};`};

  ${media.desktop`padding-right: ${props =>
    props.last ? 0 : '32px'};`} ${media.desktop`padding-left: ${props =>
    props.first ? 0 : '32px'};`};
`

let helloLocation

if (typeof location !== 'undefined') {
  helloLocation = location.search
}
const { hello } = getParams(helloLocation)

const IndexPage = props => (
  <Layout>
    <Container>
      <Columns>
        <Column first flex={1}>
          <Banner referrer={hello} />
          <Blogs />
        </Column>
        {/* <Column>
          <Blogs />
        </Column> */}
        <Column last flex={2}>
          <Works />
        </Column>
      </Columns>
    </Container>
  </Layout>
)

export default IndexPage
