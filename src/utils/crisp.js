const welcomeMessage =
  'Hey there! Great to have you here. You can write to me here or email me at consult@aswin.design.'

export let crispmessagesent = false

export const openCrisp = () => {
  $crisp.push(['do', 'chat:open'])
  if (!crispmessagesent) {
    $crisp.push(['do', 'message:show', ['text', welcomeMessage]])
    crispmessagesent = true
  }
}
