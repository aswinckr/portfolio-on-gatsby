import styled, { css } from 'styled-components'

const sizes = {
  desktop: 992,
  tablet: 768,
  phone: 576,
}

// Iterate through the sizes and create a media template
const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${sizes[label] / 16}em) {
      ${css(...args)};
    }
  `

  return acc
}, {})

const Container = styled.div`
  padding: 0 18px;
  ${media.desktop`padding: 0 calc(50% - 480px);`}
  background: ${props => (props.white ? '#FFF' : 'transparent')}
`
const BlogContainer = styled.div`
  padding: 0 18px;
  ${media.desktop`padding: 0 calc(50% - 350px);`}
  background: ${props => (props.white ? '#FFF' : 'transparent')}
`

export { BlogContainer, Container, media }
