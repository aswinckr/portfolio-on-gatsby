export function getNodesOfType(data, type) {
  const edges = data.allMarkdownRemark.edges
  const nodes = edges.filter(
    eachnode => eachnode.node.frontmatter.category == type
  )
  return nodes
}

// export function getPagesOfType(data, type) {
//   const edges = data.allPagesData
//   const nodes = edges.filter(
//     eachnode => eachnode.node.frontmatter.category == type
//   )
//   return nodes
// }

export const getParams = (search = '') => {
  return search
    .replace('?', '')
    .split('&')
    .reduce((params, keyValue) => {
      const [key, value = ''] = keyValue.split('=')
      if (key && value) {
        params[key] = value.match(/^\d+$/) ? +value : value
      }
      return params
    }, {})
}

export const next = pathName => {}
