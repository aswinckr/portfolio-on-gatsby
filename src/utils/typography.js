import Typography from 'typography'
import CodePlugin from 'typography-plugin-code'
import { MOBILE_MEDIA_QUERY } from 'typography-breakpoint-constants'
import styled from 'styled-components'

const options = {
  baseFontSize: '14px',
  baseLineHeight: 1.6,
  bodyFontFamily: ['Barlow', 'Helvetica', 'sans-serif'],
  headerFontFamily: ['Barlow', 'Helvetica', 'sans-serif'],
  scaleRatio: 3.5,
  googleFonts: [{ name: 'Barlow', styles: ['400', '700'] }],
  plugins: [new CodePlugin()],
  overrideStyles: () => ({
    [MOBILE_MEDIA_QUERY]: {
      // Make baseFontSize on mobile 16px.
      html: {
        fontSize: `${(16 / 16) * 100}%`,
      },
    },
  }),
}

const typography = new Typography(options)
const { rhythm } = typography

export { rhythm }
export default typography

// Styled Components

export const H1 = styled.h1`
  font-size: var(--h1-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--primary-font);
  margin-bottom: 1rem;
  font-weight: var(--secondary-font-regular);
`

export const H1Article = styled.h1`
  font-size: var(--h1-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--secondary-font);
`

export const H2 = styled.h2`
  font-size: var(--h2-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--primary-font);
`

export const H2Article = styled.h2`
  font-size: var(--h2-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--secondary-font);
`

export const H3 = styled.h3`
  font-size: var(--h3-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--secondary-font);
  font-weight: var(--secondary-font-regular);
  line-height: ${rhythm(1.2)};
  margin-bottom: 1.2rem;
  opacity: 0.9;
`

export const H4 = styled.h4`
  font-size: var(--h4-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--secondary-font);
`

export const H4Italic = styled.h4`
  font-size: var(--h4-text-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--secondary-font);
  font-style: italic;
  opacity: 0.5;
  font-weight: 300;
`

export const P = styled.p`
  font-size: var(--p-txt-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  font-family: var(--secondary-font);
  line-height: ${rhythm(1.2)};
  opacity: 0.8;
`

export const PBold = styled.p`
  font-size: var(--p-txt-size);
  font-family: var(--secondary-font);
  font-weight: var(--p-bold-weight);
`

export const PItalic = styled.p`
  font-size: var(--p-txt-size);
  font-family: var(--secondary-font);
  font-weight: var(--p-bold-weight);
  font-style: italic;
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
`

export const A = styled.a`
  font-family: var(--secondary-font);
  display: inline-block;
  color: var(--link-color);
  text-decoration: none;
`

export const ABlog = styled.a`
  font-family: var(--secondary-font);
  display: inline-block;
  font-size: var(--a-txt-size);
  color: ${props => (props.black ? '#000' : '#FFF')};
  text-decoration: none;
  transition: 0.3s;
  line-height: ${rhythm(1.5)};
  &:hover {
    color: var(--primary-color);
  }
`

export const HomeTitle = styled.h4`
  text-transform: uppercase;
  color: #fff;
  opacity: 0.2;
  font-weight: var(--secondary-font-light);
  margin-bottom: ${rhythm(0.6)};
`

export const ANav = styled.a`
  font-family: var(--secondary-font);
  font-size: 0.8rem;
  font-weight: var(--secondary-font-light);
  display: inline-block;
  color: var(--main-txt-color);
  opacity: 0.8;
  text-decoration: none;
  transition: 0.3s;
  margin-left: ${rhythm(1.1)};
  &:hover {
    color: var(--primary-color);
  }
`

export const Caption = styled.span`
  font-size: var(--caption-txt-size);
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
  opacity: var(--txt-opacity-dimmed);
  font-family: var(--secondary-font);
  text-align: center;
  display: block;
  font-style: italic;
  padding: 16px 0;
`

export const Ul = styled.ul`
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
`

export const Ol = styled.ol`
  color: ${props => (props.black ? '#000' : 'var(--main-txt-color)')};
`
