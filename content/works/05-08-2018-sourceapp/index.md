---
title: Finets - The social financing app
date: '2017-07-13T15:12:33.962Z'
tags:
  - finet
  - ios app
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/finets'
---

Finets are group of individuals coming together to achieve their financial aspirations by unlocking the implicit trust in their social networks.

The Finets app is powered by blockchain technology and a dynamic bidding process to create self funding circles with each member acting as both a contributor and beneficiary.

[Checout their website](https://finets.us/) for more information on the product.

## The Design Process

This was a short 2-week project to conceptualize Finets and its application through a visual prototype for pitch decks and initial product visualization. Some content is protected by an NDA.

![1](./images/journeymap.png)

![alt text](https://res.cloudinary.com/aswin/image/upload/q_auto:good/v1511799095/sourceapp/bpmn_nu8gpq.png 'IA & BPMN')

## Initial Prototype

A quick version of the intiial prototype was made for collaboration with the stakeholders. We refined on this version after we broke down the possible issues and consolidated solutions.

<iframe src="https://marvelapp.com/297gcba?emb=1&iosapp=false&frameless=false" width="100%" height="755"  allowTransparency="true" frameborder="0"></iframe>

## Final Prototype

<iframe src="https://marvelapp.com/2cc12j8?emb=1&iosapp=false&frameless=false" width="100%" height="755"  allowTransparency="true" frameborder="0"></iframe>

### Contextual Popups

![alt text](https://res.cloudinary.com/aswin/image/upload/q_auto:good/v1511781225/sourceapp/popup_cgx3dy.gif 'Popup Animation')

### Social States

![alt text](https://res.cloudinary.com/aswin/image/upload/q_auto:good/v1511778744/sourceapp/States_nwil9a.png 'States')

### Notifications

![alt text](https://res.cloudinary.com/aswin/image/upload/q_auto:good/v1511782412/sourceapp/Notification_kvtom7.gif 'Notifications')

### Interactions

![alt text](https://res.cloudinary.com/aswin/image/upload/q_auto:good/v1511705976/sourceapp/Bidding_lv7rt0.gif 'Bidding GIF')
