---
title: Framer Mastery
date: '2017-07-11T17:12:33.962Z'
tags:
  - framer
  - course
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/framer-for-beginners'
---

In March 2018 I started using Framerjs and absolutely loved the tool. It gave me the power to explore interactions with Real data and custom logic which was not possible in any other tool available. I spent a few months playing around with this tool and I decided to create a course on it so I can learn as I teach and that way I can be helpful for others as well who start taking interest in this tool. Here are a couple of courses I made -

## Build a custom calculator in Framer

<a target="_blank" href="https://framermastery.teachable.com/p/build-custom-calculator-framer?wvideo=yg9iajlydo">![1](https://www.filepicker.io/api/file/c3C8JuQNSgObHcm7kdKL)</a>
<a target="_blank" href="https://framermastery.teachable.com/p/build-custom-calculator-framer?wvideo=yg9iajlydo">See Course</a>

## Learn basics of Framer and build your first prototype

<a target="_blank" href="https://framermastery.teachable.com/p/learn-framer-basics?wvideo=zguk4hbv86">![1](https://www.filepicker.io/api/file/1SN6cRLwSpSXtrzV677l)</a>
<a target="_blank" href="https://framermastery.teachable.com/p/learn-framer-basics?wvideo=zguk4hbv86">See Course</a>

## Framer Workshop in Bangalore

I also conducted a paid workshop in Bangalore and taught about 15 designers at the Zeta HQ. Designers learnt the basics of Javascript and how to integrate custom data into their prototypes. This was a super intensive 5 hour course.

<a target="_blank" href="https://framermastery.teachable.com/p/framer-workshop-build-your-own-learning-app-from-scratch">![3](./images/framerworkshop.png)</a>

## Why I had to discontinue my course

Framer Mastery was going well and I started to see a minimum of 20 new visitors daily to my page in just a few days from starting my course. And then Framer decided to discontinue any support to their tool and even stopped issuing licenses for it in favour of FramerX. FramerX was completely different from Framer and was based on ES6 and React as against Coffeescript, which was much simpler for designers.

I have plans to work on a course on React in the future when FramerX becomes a little more stable and they add a few more helper libraries to make the tool friendly for designers.
