---
title: Send & Receive Money with Zeta
date: '2017-07-13T15:12:33.962Z'
tags:
  - zetap2p
  - web
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/zetap2p'
---

**Timeline** — 3 weeks

**Project Stage** - Live

[<img src="https://res.cloudinary.com/aswin/image/upload/q_auto:best,w_130/v1511865163/google-play-badge_eprvuo.png">](https://play.google.com/store/apps/details?id=in.zeta.android&hl=en)
[<img src="https://res.cloudinary.com/aswin/image/upload/c_scale,q_auto:best,w_120/v1511863972/App_Store_icon_q0kzj0.png">](https://itunes.apple.com/in/app/zeta-for-employees/id1034765599?mt=8)

**Tools used** — Sketch, Marvelapp

**Usage** - <img style="width:20px; position:relative; top:4px;" src="https://res.cloudinary.com/aswin/image/upload/v1513700855/star-128_tszazm.png"/> 50,000+ transactions recorded during mid-2017

**My Contribution** - I worked on the initial stages of Zeta app where as the designer, we introduced the send and receive money feature. The product shifted focus later but this feature continues to exist and remains one of the most frequently used feature in the app.

<!-- ## Prototype

The initial prototype made for send and receive money feature.

_Note: Use Arrow keys or click anywhere to navigate. This is not a click-through prototype_

<div style={{width: "100%"}}><iframe src="https://marvelapp.com/9g909b9?emb=1" width="320" height="620" allowTransparency="true" frameborder="0"></iframe></div> -->
