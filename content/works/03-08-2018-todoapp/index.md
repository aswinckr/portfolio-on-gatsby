---
title: Todo for Shared Household
date: '2017-07-12T17:12:33.962Z'
tags:
  - interview
  - todoapp
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/todo-for-shared-household'
---

This was a week long project designed and prototyped to demonstrate the UX process and outcomes. The task given was to design a todo application for any audience and prototype the results and present the research.

## Context

There are hundreds of todo applications on different app stores and every application carries out the same task objectively - Create todo's and manage them. However, in a crowded space the only way to stand out from the competition with a product is to serve a specific niche and build features around them.

Considering the time to complete this project I picked the niche of shared household because the audience I was staying with were the potential users to conduct any research for this project. I started with research and tested wireframes with marvelapp before proceeding to make visual designs.

## Research

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQqXw8ezTADN7NOQxaLFbQ66_ER_bWsfNaZlZSsY8qwmIQG4yu89MW7vBAuUodaZI2M534cVmZLZ5vn/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="369" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Interaction Video

<iframe src="https://www.youtube.com/embed/yngY5fI6P3k"  width="100%" height="369" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Prototype

<iframe src="https://marvelapp.com/2bc4262?emb=1&iosapp=false&frameless=false" width="100%" height="755"  allowTransparency="true" frameborder="0"></iframe>
