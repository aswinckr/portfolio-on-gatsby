---
title: Talent - The Hiring App
date: '2017-07-13T17:12:33.962Z'
tags:
  - zeta
  - webapp
  - merchants
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/talentapp'
---

A popular venture capital firm wanted to create a platform to manage hiring for its portfolio of companies. The end product required was a web app that is to be used by 2 parties -

1. Appointed platform Admins who review applications and shortlist based on different criteria
2. Managers from companies who review candidates based on requirements and approve or reject them

## Role

As a remote designer my role is to capture the use cases of each of the parties ( the admins and the hiring managers ) that operate this tool and facilitate the best user experience across them.
Most importantly the objective is to get the right candidates show up for the right requirement and get them hired.

## Working remotely

This project was worked on entirely remotely. We used a set of tools for facilitating collaboration -

   ![1](./images/collaboration.png)

We collaborated with the stakeholders twice a week, along with a product manager and presented our approaches. We gathered feedback and presented our assumptions to be validated. We used these insights to move forward with the design process.

# Platform Goals

The objective of the tool was to work in a 3 step process.

1. Applied candidate profiles float in the platform waiting for admins to take action
2. Admins shortlist these candidates based on their qualifications
3. Managers look up and find candidates based on requirement and approve or reject them for their role

# Design challenge

**For the admins -**
Product experience for reviewing tens or hundreds of applications, looking up their information and shortlisting them.

**For the managers -**
Searching and finding candidates matching for respective requirements and approving or rejecting them based on their skill sets.

# Personas

The tool was designed for two main persona’s representing the admin and the manager of the company. *Their goals and pain points were mostly assumed* ( PS. Drawbacks for working with remote clients ). But our stakeholders presented a clear case of their responsibilities

# Competitor products

![4](./images/competitor-1.png)

--

![5](./images/competitor-2.png)

## Task Flows - For applicants

1. Candidates discover the job application site on social media or a word of mouth referral
2. Candidate fills out a typeform that captures all the work related information about candidate
3. Information about the candidate along with his name and other personal details get recorded in the system waiting to be **shortlisted** by an admin

## Task Flows - For admins

1. Looks at each profile from a list of candidates who have applied for the role
2. Reviews different work related information entered from the typeform ( on a separate dashboard, not typeform results )
3. Shortlists candidate based on his / her credibility and relevance to the possible requirements at companies

## Task Flows - For managers

1. Search and find candidates based on requirement and create a list
2. Review each profile in the list
3. Approve or reject candidate based on match

# The 3-sided product

![6](./images/ia.jpg)

We carefully picked terminology like Applied to Screening, Shortlisted to Live, Archived to Inactive and added a tab called mentioned inspired from Facebook notifications to highlight all profiles that have the specific user mentioned.

## Profile Views

![7](./images/profileview.png)

## Admin Checklist

![8](./images/adminchecklist.png)

## Facilitating teams
In the current ways of communication within teams, the managers would find a profile and often had to discuss with a few others on the team before deciding to move forward. The primary communication today for this purpose happens through email. When profiles are discovered on the platform, switching back and forth between email for communication and the tool for taking actions was inconvenience.

“Comments” helped solve both problems in the platform. For future releases we provisioned visibility of these comments for within companies if required.

![9](./images/facilitatingteams.png)

## Profile states

![11](./images/profilestates.png)

![12](./images/profilestates-2.png)

## Retrieving with Labels

![13](./images/labels-1.png)

## On Mobile

![14](./images/labels-2.png)

## Manager View

![15](./images/labels-3.png)

![16](./images/labels-4.png)

# Interaction Design

## Viewing a profile

![17](./images/profileview.png)

## Shortlisting a profile

![18](./images/shortlist.gif)

## Setting alerts

![19](./images/alerts.png)

## Emailers

![20](./images/customerjourney-1.png)

![21](./images/customerjourney-2.png)
