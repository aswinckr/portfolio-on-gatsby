---
title: Crazy Egg Short Case Study
date: '2017-07-12T15:12:33.962Z'
tags:
  - crazyegg
  - web
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/crazyegg'
---

I worked on a very short paid assignment for Crazy egg as a part of their remote job interview process and I was one of the three applicants to make it to final round of the interview among many. The remote designer role however was extended to another candidate almost thice as experienced than me ( to the tune of 13 years ).
<br>
<br><br>
<a href="https://paper.dropbox.com/doc/Crazyegg-Case-Study--AT8owY73UJnl_FDtf0Ya_502AQ-I45gHa6lA1tIZijbZ2duo" class="redirect-button">Click to see detailed case study </a>

<br>
<br>
<br>
<br>
