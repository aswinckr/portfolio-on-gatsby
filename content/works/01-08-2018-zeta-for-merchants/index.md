---
title: Zeta for Cafeteria Merchants
date: '2017-07-14T17:12:33.962Z'
tags:
  - zeta
  - webapp
  - merchants
category: work
cover: './images/cover.png'
thumbnails: ['thumbnail.png']
path: '/zeta-for-merchants'
---

Zeta for Cafeteria merchants is a web app used by cafeteria merchants as

1. A POS ( Point of Sale ) solution for enterprise cafeteria merchants to receive and accept payments
2. Platform for managing cafeteria products like adding and removing food items, marking items out of stock and order management
3. A platform to manage sales and settlement reports and assign roles to employees and regulate their access across the platform

## Design scope

1. Food inventory management redesign
2. Managing sales and settlements after successful transactions from customers
3. Defining merchant access roles to restrict visibility to sections of the app

## Business Context

- In 2014 there were 3million IT employees in India and that has been rising exponentially since. Today there are thousands of startups and many other companies most of them work out of a building that where multiple companies work together called as an IT park or from co-working spaces.
- Zeta, a startup started by the popular Directi parent company saw an opportunity here. At Zeta, we built an app that provided employees a part of their salary as a tax free component that they can spend on food and beverages. At the beginning of every month each employee receives a part of their income as zeta meal vouchers and they can redeem these vouchers in their cafeterias and pay for their lunch.
- In 2017, I was hired as the designer for a product that allows waiters at the cafeteria counter to add and modify food items, see orders and receive payments and monitor their sales and settlements from one dashboard. Users would then be able to order from the order section on the zeta app.

By Feb 2018, close to 10Million monthly transactions has been managed from this product.

<!-- https://www.dropbox.com/s/z501pcqx5fitrca/Testimonials_Cisco_Cafeterias.mp4?dl=0 -->


# Who are our users?

The users of this platform were cafeteria merchants. Personas involved varied education and role levels based on the company structure but the design challenge mainly revolved around designing for -

- Non-english speaking / semi-literate workers
- Managers who assign roles and monitor revenue
- Business owners who run these businesses who hire managers and monitor settlements


## User Interviews

We already had an MVP of this tool with bare minimum functionality. Our goal was to extend its functionality and tailor into an scalable enterprise product. Rapid questioning helped us understand their behaviour patterns.

![1](./images/zeta-user-interviews.png)

- Can you teach me how to operate this tool?
- Do you face any problems when there are long queues
- Have you ever had to call the manager for any issues you faced?
- Have you let someone else operate this in your absence and do you remember any issues that they faced?
- Did you add all the food items inside it?
- How do you handle cases when any/all items become unavailable?
- How do users know when can they start ordering in the mornings?
- Do you sometimes find the tool freeze or unusable at times of ordering? Does that cause any problems
- Does any one else operate the same tool simultaneously?

## Video Walkthrough of current tool

https://www.dropbox.com/s/ja44ngo51sayh1m/Menu_management_old.mov?dl=0

## Problems

Some of the highlighted problems we wanted to tackle in this redesign were -

1. Searching and finding menu items to modify from a list was a pain. There was no quick search and merchants are not very fluent with their spellings to type and search.
2. Switching between different cafeterias, managing store timings and role management were required features but unavailable
3. Zeta's charges and revenue generated by merchants needed to be presented clearly on a dashbaord for monitoring
4. Interface had to tailor to enterprise requirements like downloadable reports etc.

## Competitor Products

Before getting into any design work, I visited a few restaurants to check their menu management solution offered by existing businesses like [Swiggy](https://swiggy.com) and [Zomato](https://zomato.com). These businesses have similar products but used by restaurants to receive and process food delivery requirements. Though the use cases are very different these products did serve as a source of inspiration.

# Interaction Design

## Category and Item Navigation

![7](./images/zeta-navigation-2.png)

## Availability Toggling

![9](./images/availability-toggle-2.gif)

## Bulk entry of food items

![11](./images/multiple-items-2.gif)

# Sales and settlement reports

## Sales Report

![14](./images/ia-3.png)

## Settlement Report

![15](./images/ia-4.png)

## Navigation from the main application

![13](./images/ia-2.png)

## Defining Access Roles

Typically cafeterias had multiple people operating and after studying different different merchants we decided to model the platform into three roles -

1. Business Owner
2. Store Manager
3. Counter Operator

![16](./images/access-role-1.jpg)

![17](./images/access-role-2.jpg)

<!-- # Prototype

We made a quick prototype on Marvelapp with basic features to present to the stakeholders.

https://marvelapp.com/9gdcd7j -->

# Making it responsive

![18](./images/responsive-1.png)

![19](./images/responsive-2.png)

# Results

We tested the product while building it with our audience to understand if it works as expected. Thankfully 1.5 years later the product works as expected and records more than 10 Mil transactions monthly.

## Would you like to read something else?
