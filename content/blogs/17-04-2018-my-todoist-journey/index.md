---
title: My Todoist Journey
date: '2018-04-17T18:49:33.962Z'
path: '/my-todoist-journey'
category: blog
cover: './images/cover.png'
tags:
  - learning
  - code
  - design
thumbnails: ['thumbnail.png']
---

I’ve been a Todoist user for a few months now and in this blog I wanted to write about my experience with it and share how it changed my life. For a very long time I managed my daily tasks with the default notes app on the iPhone or in some cases a simple paper and pencil. Most of the times it worked perfectly until I landed into the obvious problems it couldn’t address -

- It was hard to retrieve notes from the past
- Its almost pointless to make notes for the future
- There was no way to organise these notes in any way
- I almost always lose them

After much thought, I decided to pay for a todo application for the first time ever and see how it would change my lifestyle. After exploring a few options out there like Wunderlist, Things for Mac and a few others I decided to pick Todoist mainly because it was cross-platform. Having moved to an Android phone but with a Mac laptop, adding tasks from any device and viewing them the others was a necessity for me. After I purchased and downloaded Todoist, I tried a couple of experiments with the way I adopted it into my lifestyle and the results in each case turned out very differently.

## My Todoist

![1](./images/mytodoist.png)

At the time of writing, I reached the master level in the app by completing 755 tasks. Todoist has a gamification system that rewards with points for completing tasks and this was one of my favourite features in the app. To get an idea of the different karma levels you can take a look at the chart below -

![1](./images/karmalevels.png)

## My first experiment

Once I had the tool, the next step was to add tasks and figure out a way to organize them to the app. My obvious approach was that when I had a project to complete, I usually just broke them down into about 4-5 smaller tasks and assigned a due date to each of those. I also used the 'Projects' feature to create several categories of tasks. I even had a 'Personal' Project which had tasks like Practice my violin, Cook a new dish, go to the gym etc. I felt good about the things I would do during the day and all I had to do now was to now simply take each of them and get them done.

What mattered was checking things off and reaching my daily goal of 7 tasks per day ( Todoist default is 5 ). Indeed this was working well when I started off. I used to wake up in the morning, check the list of things I need to do, pick up the most important task for the day and start checking things off until I finished them all. Sounds like just the thing to make your day perfect, right? But this is when it started to become painful instead of helping in any way.

There was a pattern I observed with how my work evolved with the tool. I, like many other people I suppose, didn’t finish all the tasks assigned on a specific day as you might have expected. I had to postpone tasks if I spent more time on an important task and this was most of the time. I made sure I had 2-3 important tasks through the day and other less important pending tasks would come to about 6-7 more ( Most of these would end up being my personal tasks than work related ). On top of this, I got exhausted by the end I completed all the important tasks and I had hardly any time or energy left for the others.

But the purpose of using a Todo application itself is to get stuff out of your head right? Leave you with no anxiety and keep your brain stress free. The way I was operating my Todoist was working otherwise on me. I was anxious most of the time about the tasks I needed to complete. At any point, I knew I had a bunch of things on my task list to be completed and even if I decided to take a break and meet my friends, it would keep bothering me that there still were tasks pending.

So to break this, I started clearing my 'Today' section and pushing it to the next day before I went out to eat or meet my friends. I would then feel slightly better that my day was free to have fun. This was okay but all I did was postponing and it became an everyday routine. Every day I had a bunch of tasks to postpone before I hit the bed. If I didn't take care of it, it goes into overdue and the next day becomes even more stressful. Sometimes I would have even completed my tasks but I would forget to check it off and it would appear on my overdue the next day mildly adding to my stress levels. Just looking at 'Overdue' started to stress me out over a period of time.

## The change

I needed a different approach. I realised I was doing this wrong all along. The feeling of having my Today section cleared up was great and I needed more of that. I was happier and I was able to relax better. These were the changes I did -

1. I removed all the due dates and kept my Today section empty every morning. All the tasks lived inside Project folders without a due date.
2. I removed all the routine tasks like go to the gym or drink water. I knew these were my routines and I needed to do it anyway but listing them as a task fundamentally changed the way I saw them. It was not a fun anymore, It was a to-do thing. That sucked.
3. I segregated tasks inside Project folder in order of completion for me to get something done. I knew I would sit to work daily at a specific time. I would then browse through the list of projects and decide which project related task I need to do at that point and add a due date to the first one to today. I would then complete and check it off. If I didn't have the time to complete it, I would take a call and postpone the remaining right then and there.

This helped me decide how my day was going to be based on my mood or feeling when I woke up every morning which was great. I stopped letting Todoist control me and let me control myself instead. If I added nothing today, my today section had the nice illustration saying no tasks for today.. and it felt great even though its just a no tasks added. I could optionally pick something to work on. This control felt great. Psychologically it was a win.

I wrote this hoping this would be helpful to some of you if you find it hard to stick to any productivity tools and take no benefit from it. This method worked well for me and really impacted my productivity and I hope it does the same for you too.

Thankyou for reading!
