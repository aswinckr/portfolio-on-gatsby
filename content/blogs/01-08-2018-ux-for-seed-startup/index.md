---
title: UX for a seed stage startup
date: '2017-07-12T17:12:33.962Z'
path: '/ux-for-a-seed-stage-startup'
category: blog
cover: './images/cover.jpeg'
tags:
  - learning
  - code
  - design
thumbnails: ['thumbnail.png']
---

Almost every startup today needs at least one UI & UX designer to build usable products and beautify interfaces. But what is it to an early stage startup when your funds are limited? Do you really need a full time UX guy at your office? I’m going to demystify things a bit for your understanding.

## When you hire your first UX guy

You have an idea, you have done your market research, and you are pretty convinced that your product is going to make big if done right. Its time for you to now take those ideas and extend it into an app or a website ( I say extend because you may have operations / logistics / delivery etc. that runs behind the app, but more about this later) and yes, the first thing you look for is someone make those app screens for you.

Now you go into dribbble and start looking at designers with catchy work or maybe you ask in your network and you will end up finding one. You see his/her portflolio with neat looking UIs or a few apps on the app store that makes you think — “Let me try and contact him/her. I know what each screen should be like and I’ll tell them what they are and I’ll get the job done”. Well, maybe you’re right. Maybe you can get the screens you imagined but you have effectively made use of maybe only 20% of the skills of a UX Designer that you are paying for. Or if you go simply find a UI designer you are betting on your own skills of a UX Designer.

## The kind of designer you need

Designers out there more often specialise in one of the two — UX or UI, sometimes both. For context, UX designers are those who have learnt and practiced designing as a process from brainstorming, market & user research and so on till wireframes and maybe a low fidelity / high fidelity UI. Some UX designers simply make wireframes and pass it on to a UI designer, but this requires two people working on the product and this mostly happens in a funded company that can afford two or more designers working full time. The good part is you don’t need two or more designers to get your MVP made.

Let me explain why - More often the MVP you make, even if you’ve made it from one of the most experienced or popular designer is going to be WRONG. By wrong i mean there could be one of the possible issues with the product -

- The user understands and appreaciates the product quite well but then your business takes a change in direction and needs to accommodate / refactor a thing or two in the information architecture after you’ve realised something isn’t working as expected
- The designer has provided you with some details which are critical to the product flow but you cut them out during development because of timeline constraints and push it to the next release cycle. Or if the designer is inexperienced, you may end up in even worse situations
- The designer fails to take care of the education piece at the right time in the flow having taken wrong or missing assumptions of the user mental model leaving the user confused on what to do

Inexperienced designers usually come with no coding knowledge and fail to take into consideration the efforts it takes to build something and choose creating impressive something over achieving the same results with simpler approaches

You can almost never get the product right in your first design iteration because you make several assumptions on the mental model of the user.

When you are an early stage startup, quick and rapid iterations help you learn about your users faster and better (established companies that have data and time for an extensive user research). So when you know you have to work on your product again and again you begin to see the need for an in-house designer, or I’m going to point you to another solution.

You need a designer who can make super fast wireframes that are easiest in terms of development efforts (so you can go launch as quick as possible) and deliver with a UI that follows standard trends and patterns ( so you can take advantage of your users familiarity with interfaces )

## The Proposal

The problem is now — where can I find that kind of designer. Let me tell you clearly that its hard today. Its hard to evaluate a designer to match this kind of need or work with a freelance designer sitting at his desk elsewhere and get them listen to how they should do their job.
This is when you can get smart and break the process into two —

Working with a UX Consultant to make sure you effectively apply most of his learning in crafting user experiences with a process
Outsourcing UI design with the help of the UX consultant to get your fast and effective product done at a fraction of the time and cost you would have ended up paying.

I would like to hear your thoughts on the above article and you can write to me at consult@aswin.design or respond in the comments below.
